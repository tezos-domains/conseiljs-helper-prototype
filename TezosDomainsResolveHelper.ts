import { TezosMessageUtils, TezosNodeReader } from 'conseiljs';
import { JSONPath } from 'jsonpath-plus';

export interface NameRegistrySimpleStorage {
    recordMap: number;
    expiryMap: number;
    reverseRecordMap: number;
}

export interface DomainInfo {
    address: string | null;
    owner: string;
    expiry: Date;
    data: Record<string, string>;
}

export interface ReverseRecordInfo {
    name: string | null;
    owner: string;
    data: Record<string, string>;
}

export namespace TezosDomainsResolveHelper {
    export async function getSimpleStorage(server: string, address: string): Promise<NameRegistrySimpleStorage> {
        const storageJson = await TezosNodeReader.getContractStorage(server, address);

        return {
            recordMap: Number(JSONPath({ path: '$.args[0].args[1].args[0].args[1].args[1].int', json: storageJson })[0]),
            expiryMap: Number(JSONPath({ path: '$.args[0].args[1].args[0].args[0].args[1].int', json: storageJson })[0]),
            reverseRecordMap: Number(JSONPath({ path: '$.args[0].args[1].args[1].args[0].int', json: storageJson })[0]),
        };
    }

    export async function resolveDomainRecord(server: string, storage: NameRegistrySimpleStorage, name: string): Promise<DomainInfo | null> {
        const info = await getValidRecord(server, storage, name);

        if (!info) {
            return null;
        }

        return {
            address: info.record.address,
            data: info.record.data,
            owner: info.record.owner,
            expiry: info.expiry,
        };
    }

    export async function resolveNameToAddress(server: string, storage: NameRegistrySimpleStorage, name: string): Promise<string | null> {
        const domain = await resolveDomainRecord(server, storage, name);

        return domain?.address || null;
    }

    export async function resolveReverseRecord(server: string, storage: NameRegistrySimpleStorage, address: string): Promise<ReverseRecordInfo | null> {
        const reverseRecord = await getReverseRecord(server, storage.reverseRecordMap, address);

        if (!reverseRecord) {
            return null;
        }

        const record = await getValidRecord(server, storage, reverseRecord.name);
        if (!record) {
            return null;
        }

        return reverseRecord;
    }

    export async function resolveAddressToName(server: string, storage: NameRegistrySimpleStorage, address: string): Promise<string | null> {
        const reverseRecord = await resolveReverseRecord(server, storage, address);

        return reverseRecord?.name || null;
    }

    async function getValidRecord(server: string, storage: NameRegistrySimpleStorage, name: string) {
        const record = await getDomainRecord(server, storage.recordMap, name);

        if (!record) {
            return null;
        }

        const expiry = await getDomainExpiry(server, storage.expiryMap, record.expiry_key);

        if (expiry && expiry < new Date()) {
            return null;
        }

        return { record, expiry };
    }

    async function getDomainRecord(server: string, mapid: number, name: string): Promise<DomainRecord | null> {
        const mapKey = TezosMessageUtils.encodeBigMapKey(Buffer.from(TezosMessageUtils.writePackedData(encodeString(name), 'bytes'), 'hex'));

        const mapResult = await TezosNodeReader.getValueForBigMapKey(server, mapid, mapKey);

        if (!mapResult) {
            return null;
        }

        return {
            address: JSONPath({ path: '$.args[0].args[0].args[0].args[0].string', json: mapResult })[0],
            owner: JSONPath({ path: '$.args[1].args[0].args[1].string', json: mapResult })[0],
            expiry_key: JSONPath({ path: '$.args[0].args[1].args[0].args[0].bytes', json: mapResult })[0],
            data: dataToObj(JSONPath({ path: '$.args[0].args[0].args[1]', json: mapResult })[0]),
        };
    }

    async function getReverseRecord(server: string, mapid: number, address: string): Promise<ReverseRecordInfo> {
        const mapKey = TezosMessageUtils.encodeBigMapKey(Buffer.from(TezosMessageUtils.writePackedData(address, 'address'), 'hex'));

        const mapResult = await TezosNodeReader.getValueForBigMapKey(server, mapid, mapKey);

        if (!mapResult) {
            return null;
        }

        return {
            name: decodeString(JSONPath({ path: '$.args[1].args[0].args[0].bytes', json: mapResult })[0]),
            owner: JSONPath({ path: '$.args[1].args[1].string', json: mapResult })[0],
            data: dataToObj(JSONPath({ path: '$.args[0].args[0]', json: mapResult })[0]),
        };
    }

    async function getDomainExpiry(server: string, mapid: number, expiryKey: string | null): Promise<Date | null> {
        if (!expiryKey) {
            // record never expires
            return null;
        }

        const mapKey = TezosMessageUtils.encodeBigMapKey(Buffer.from(TezosMessageUtils.writePackedData(expiryKey, 'bytes'), 'hex'));

        const mapResult = await TezosNodeReader.getValueForBigMapKey(server, mapid, mapKey);

        return mapResult ? new Date(mapResult.string) : null;
    }

    function dataToObj(michelsonData: any[]) {
        return michelsonData.reduce((result, item) => {
            const key = JSONPath({ path: '$.args[0].string', json: item })[0];
            const value = JSONPath({ path: '$.args[1].bytes', json: item })[0];

            if (value) {
                result[key] = value;
            }

            return result;
        }, {});
    }

    function encodeString(str: string): string {
        let result = '';
        const encoded = new TextEncoder().encode(str);
        for (let i = 0; i < encoded.length; i++) {
            const hexchar = encoded[i].toString(16);
            result += hexchar.length == 2 ? hexchar : '0' + hexchar;
        }
        return result;
    }

    function decodeString(hexString: string): string {
        return new TextDecoder().decode(hexToArray(hexString));
    }

    function hexToArray(hexString: string): Uint8Array {
        return new Uint8Array(hexString.match(/.{1,2}/g)?.map(byte => parseInt(byte, 16)) || []);
    }

    interface DomainRecord {
        address: string;
        owner: string;
        expiry_key: string;
        data: any;
    }
}
