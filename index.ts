import { registerFetch, registerLogger } from 'conseiljs';
import fetch from 'node-fetch';
import * as log from 'loglevel';

import { TezosDomainsResolveHelper } from './TezosDomainsResolveHelper';

async function run() {
    registerFetch(fetch);
    registerLogger(log);

    const server = 'https://delphinet-tezos.giganode.io/';
    const address = 'KT1CR6vXJ1qeY4ALDQfUaLFi3FcJJZ8WDygo';

    const storage = await TezosDomainsResolveHelper.getSimpleStorage(server, address);
    const domain = await TezosDomainsResolveHelper.resolveDomainRecord(server, storage, '1337.delphi');

    console.log(domain);
    console.log(await TezosDomainsResolveHelper.resolveNameToAddress(server, storage, 'necroskillz.delphi'));
    console.log(await TezosDomainsResolveHelper.resolveNameToAddress(server, storage, '404.delphi'));
    console.log(await TezosDomainsResolveHelper.resolveNameToAddress(server, storage, 'moral-title.delphi'));

    const reverseRecord = await TezosDomainsResolveHelper.resolveReverseRecord(server, storage, 'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n');

    console.log(reverseRecord);
    console.log(await TezosDomainsResolveHelper.resolveAddressToName(server, storage, 'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n'));
    console.log(await TezosDomainsResolveHelper.resolveAddressToName(server, storage, 'tz1S8U7XJU8vj2SEyLDXH25fhLuEsk4Yr1wZ'));
}

void run();
